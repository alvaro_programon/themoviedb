//
//  Api.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

class Api
{
    static var popularUrl = "https://api.themoviedb.org/3/movie/popular?api_key=34738023d27013e6d1b995443764da44"
    static var topRatedUrl = "https://api.themoviedb.org/3/movie/top_rated?api_key=34738023d27013e6d1b995443764da44"
}
