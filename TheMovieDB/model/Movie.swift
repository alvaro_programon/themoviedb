//
//  Movie.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

import Foundation

struct Movie
{
    var vote_count: Int?
    var id: Int?
    var video: Bool?
    var vote_average: Double?
    var title: String?
    var popularity: Double?
    var poster_path: String?
    var original_language: String?
    var original_title: String?
    var genre_ids: [Int]?
    var backdrop_path: String?
    var adult: Bool?
    var overview: String?
    var release_date: String?
}
