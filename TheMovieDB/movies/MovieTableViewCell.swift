//
//  MovieTableViewCell.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell
{
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
