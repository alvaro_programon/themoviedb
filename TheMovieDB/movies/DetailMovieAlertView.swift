//
//  DetailMovieAlertView.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

import UIKit
import Imaginary

class DetailMovieAlertView: UIViewController
{
    var movie:Movie!
    var delegate: DetailMovieAlertViewDelegate?
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var txtVoteCount: UILabel!
    @IBOutlet weak var txtOverview: UITextView!
    @IBOutlet weak var txtPopularity: UILabel!
    @IBOutlet weak var btnAceptar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadMovieData()
    }
    
    @IBAction func actionBtnAceptar(_ sender: Any)
    {
        delegate?.dismissAlert()
    }
    
    func loadMovieData()
    {
        txtTitle.text = movie.title
        if let poster_path = URL(string: movie.poster_path!)
        {
            imgMovie.setImage(url: poster_path)//imaginary
        }
        cosmosView.rating = Double(movie.vote_average!)
        txtVoteCount.text = "\(movie.vote_count!)"
        txtPopularity.text = "\(movie.popularity!)"
        
        let release_date = movie.release_date!
        let release_date_array = release_date.components(separatedBy: "-")
        txtOverview.text = "(\(release_date_array[0])) \(movie.overview!)"
        self.txtOverview.scrollRangeToVisible(NSRange(location:0, length:0))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    
    func setupView()
    {
        alertView.layer.cornerRadius = 10
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func animateView()
    {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
}
