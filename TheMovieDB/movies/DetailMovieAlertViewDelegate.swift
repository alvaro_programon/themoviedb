//
//  DetailMovieAlertViewDelegate.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

protocol DetailMovieAlertViewDelegate: class
{
    func dismissAlert()
}
