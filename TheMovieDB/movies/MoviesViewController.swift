//
//  MoviesViewController.swift
//  TheMovieDB
//
//  Created by Álvaro Patricio Agüero Vera on 2/21/19.
//  Copyright © 2019 Programación de Álvaro. All rights reserved.
//

import UIKit
import Imaginary

class MoviesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var orderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var moviesTableView: UITableView!
    var currentMoviesArray = [] as [Movie]
    var popularMoviesArray = [] as [Movie]
    var topRatedMoviesArray = [] as [Movie]
    var detailMovieAlert: DetailMovieAlertView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        moviesTableView.dataSource = self
        moviesTableView.delegate = self
        
        loadPopularMovies()
        loadTopRatedMovies()
    }
    
    func loadPopularMovies()
    {
        let url = URL(string: Api.popularUrl)
        
        let session = URLSession.shared
        var request = URLRequest(url: url!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do
            {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                {
                    if let results: NSArray = json["results"] as? NSArray
                    {
                        self.popularMoviesArray.removeAll()
                        if results.count > 0
                        {
                            for b in 0...results.count-1
                            {
                                let result_ = results[b] as! [String:Any]
                                let vote_count: Int! = result_["vote_count"] as? Int ?? 0
                                let id: Int! = result_["id"] as? Int ?? 0
                                let video: Bool! = result_["video"] as? Bool ?? false
                                let vote_average: Double! = result_["vote_average"] as? Double ?? 0.0
                                let title: String! = result_["title"] as? String ?? "Sin Título"
                                let popularity: Double! = result_["popularity"] as? Double ?? 0.0
                                let poster_path: String! = "https://image.tmdb.org/t/p/w500\(result_["poster_path"] as? String ?? "")"
                                let original_language: String! = result_["original_language"] as? String ?? ""
                                let original_title: String! = result_["original_title"] as? String ?? ""
                                let genre_ids: [Int] = result_["genre_ids"] as? [Int] ?? []
                                let backdrop_path: String! = "https://image.tmdb.org/t/p/w500\(result_["backdrop_path"] as? String ?? "")"
                                let adult: Bool! = result_["adult"] as? Bool ?? false
                                let overview: String! = result_["overview"] as? String ?? ""
                                let release_date: String! = result_["release_date"] as? String ?? ""
                                
                                self.popularMoviesArray.append(Movie(vote_count: vote_count,id: id,video: video,vote_average: vote_average,title: title,popularity: popularity,poster_path: poster_path,original_language: original_language,original_title: original_title,genre_ids: genre_ids,backdrop_path: backdrop_path,adult: adult,overview: overview,release_date: release_date))
                            }
                            DispatchQueue.main.async
                                {
                                    self.currentMoviesArray = self.popularMoviesArray
                                    self.moviesTableView.reloadData()
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async
                                {
                                    let alert_empty = UIAlertController(title: "Sin Películas", message: "No hay películas populares por ahora...", preferredStyle: .alert)
                                    alert_empty.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                                    alert_empty.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadPopularMovies()}))
                                    self.present(alert_empty, animated: true)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async
                            {
                                let alert_error = UIAlertController(title: "Error", message: "Ocurrió un problema con la carga de las películas populares...", preferredStyle: .alert)
                                alert_error.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                                alert_error.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadPopularMovies()}))
                                self.present(alert_error, animated: true)
                        }
                    }
                }
            }
            catch let error
            {
                print(error.localizedDescription)
                DispatchQueue.main.async
                    {
                        let alert_error = UIAlertController(title: "Error", message: "Ocurrió un problema con la carga de las películas populares...", preferredStyle: .alert)
                        alert_error.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                        alert_error.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadPopularMovies()}))
                        self.present(alert_error, animated: true)
                }
            }
        })
        task.resume()
    }
    
    func loadTopRatedMovies()
    {
        let url = URL(string: Api.topRatedUrl)
        
        let session = URLSession.shared
        var request = URLRequest(url: url!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do
            {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                {
                    if let results: NSArray = json["results"] as? NSArray
                    {
                        self.topRatedMoviesArray.removeAll()
                        if results.count > 0
                        {
                            for b in 0...results.count-1
                            {
                                let result_ = results[b] as! [String:Any]
                                let vote_count: Int! = result_["vote_count"] as? Int ?? 0
                                let id: Int! = result_["id"] as? Int ?? 0
                                let video: Bool! = result_["video"] as? Bool ?? false
                                let vote_average: Double! = result_["vote_average"] as? Double ?? 0.0
                                let title: String! = result_["title"] as? String ?? "Sin Título"
                                let popularity: Double! = result_["popularity"] as? Double ?? 0.0
                                let poster_path: String! = "https://image.tmdb.org/t/p/w500\(result_["poster_path"] as? String ?? "")"
                                let original_language: String! = result_["original_language"] as? String ?? ""
                                let original_title: String! = result_["original_title"] as? String ?? ""
                                let genre_ids: [Int] = result_["genre_ids"] as? [Int] ?? []
                                let backdrop_path: String! = "https://image.tmdb.org/t/p/w500\(result_["backdrop_path"] as? String ?? "")"
                                let adult: Bool! = result_["adult"] as? Bool ?? false
                                let overview: String! = result_["overview"] as? String ?? ""
                                let release_date: String! = result_["release_date"] as? String ?? ""
                                
                                self.topRatedMoviesArray.append(Movie(vote_count: vote_count,id: id,video: video,vote_average: vote_average,title: title,popularity: popularity,poster_path: poster_path,original_language: original_language,original_title: original_title,genre_ids: genre_ids,backdrop_path: backdrop_path,adult: adult,overview: overview,release_date: release_date))
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async
                                {
                                    let alert_empty = UIAlertController(title: "Sin Películas", message: "No hay películas top rated por ahora...", preferredStyle: .alert)
                                    alert_empty.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                                    alert_empty.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadTopRatedMovies()}))
                                    self.present(alert_empty, animated: true)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async
                            {
                                let alert_error = UIAlertController(title: "Error", message: "Ocurrió un problema con la carga de las películas top rated...", preferredStyle: .alert)
                                alert_error.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                                alert_error.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadTopRatedMovies()}))
                                self.present(alert_error, animated: true)
                        }
                    }
                }
            }
            catch let error
            {
                print(error.localizedDescription)
                DispatchQueue.main.async
                    {
                        let alert_error = UIAlertController(title: "Error", message: "Ocurrió un problema con la carga de las películas top rated...", preferredStyle: .alert)
                        alert_error.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                        alert_error.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: {(alert: UIAlertAction!) in self.loadTopRatedMovies()}))
                        self.present(alert_error, animated: true)
                }
            }
        })
        task.resume()
    }
    
    @objc func acionBtnMore(sender: UIButton)
    {
        self.detailMovieAlert = self.storyboard?.instantiateViewController(withIdentifier: "DetailMovieAlertView") as? DetailMovieAlertView
        self.detailMovieAlert?.providesPresentationContextTransitionStyle = true
        self.detailMovieAlert?.definesPresentationContext = true
        self.detailMovieAlert?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.detailMovieAlert?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.detailMovieAlert?.delegate = self as DetailMovieAlertViewDelegate
        self.detailMovieAlert?.movie = self.currentMoviesArray[sender.tag]
        self.present(self.detailMovieAlert!, animated: true, completion: nil)
    }
    
    @IBAction func actionOrderSegmentedControl(_ sender: Any)
    {
        if orderSegmentedControl.selectedSegmentIndex == 0 //popular
        {
            currentMoviesArray = popularMoviesArray
        }
        else //top rated
        {
            currentMoviesArray = topRatedMoviesArray
        }
        moviesTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieTableViewCell
        cell.txtTitle.text = currentMoviesArray[indexPath.row].title
        if let backdrop_path = URL(string: currentMoviesArray[indexPath.row].backdrop_path!)
        {
            cell.imgMovie.setImage(url: backdrop_path)//imaginary
        }
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(acionBtnMore), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMoviesArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}
extension MoviesViewController: DetailMovieAlertViewDelegate
{
    func dismissAlert()
    {
        DispatchQueue.main.async
            {
                self.detailMovieAlert?.dismiss(animated: true, completion: nil)
        }
    }
}
